#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query_capacity, struct block_header* block) {
    return block->capacity.bytes >= query_capacity;
}
static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count( mem );
}

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size(size_t query_size) {
    return size_max( round_pages( query_size ), REGION_MIN_SIZE );
}

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком*/
static struct region alloc_region  ( void const * addr, size_t query_capacity ) {
    block_capacity size = (block_capacity){.bytes=query_capacity};
    size_t region_size = region_actual_size(size_from_capacity(size).bytes);
    void* region_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
    struct region new_region= (struct region) {.size=region_size, .extends=true};
    if (region_addr == MAP_FAILED) {
        region_addr = map_pages(addr, region_size, 0);
        if (region_addr == MAP_FAILED) {
            return REGION_INVALID;
        }
        new_region.extends = false;
    }
    new_region.addr = region_addr;
    block_init(region_addr, (block_size) {region_size}, NULL);
    return new_region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header* start_region = HEAP_START;
    while(start_region) {
        size_t l = 0;
        struct block_header* start_reg = start_region;
        while (start_region -> next == block_after(start_region)) {
            l += size_from_capacity(start_region->capacity).bytes;
            start_region = start_region -> next;
        }
        l += size_from_capacity(start_region->capacity).bytes;
        start_region = start_region -> next;
        int res = munmap(start_reg, l);
        if (res == -1) {
            exit(0);
        }

    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query_capacity) {
  return block-> is_free && query_capacity + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query_capacity ) {
    if (!block_splittable(block, query_capacity)) {
        return false;
    }
    block_capacity capacity_new_block = (block_capacity) {.bytes = 0};
    capacity_new_block.bytes = size_max(BLOCK_MIN_CAPACITY, query_capacity);

    void* second_block_addr = block -> contents + capacity_new_block.bytes;

    block_init(second_block_addr, (block_size){size_from_capacity(block->capacity).bytes - size_from_capacity(capacity_new_block).bytes}, block -> next);
    block_init(block, size_from_capacity(capacity_new_block), second_block_addr);

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    struct block_header* next_block = block -> next;
    if(!next_block || !mergeable(block, next_block)) {
        return false;
    }
    block_size size_new_block = (block_size) {size_from_capacity(block -> capacity).bytes +
                                                      size_from_capacity(next_block -> capacity).bytes};
    block_init(block, size_new_block, next_block -> next);
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {
    struct block_search_result result;
    if (!block) {
        return (struct block_search_result) {.type = BSR_CORRUPTED};
    }
    while(block -> next) {
        while (try_merge_with_next(block));
        if (block -> capacity.bytes >= sz && block -> is_free) {
            return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
        }
        block = block -> next;
    }

    if (block -> capacity.bytes >= sz && block -> is_free) {
        return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
    }

    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query_capacity, struct block_header* block ) {
    struct block_search_result result = find_good_or_last(block, query_capacity);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query_capacity);
        result.block -> is_free = false;
    }
    return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query_capacity ) {
    if (last == NULL) {
        return NULL;
    }

    size_t size_region = query_capacity;

    void* addr_new_region = (uint8_t* )last + size_from_capacity(last -> capacity).bytes;
    //void* addr_new_region = last->contents + last->capacity.bytes;
    const struct region new_region = alloc_region(addr_new_region, size_region);
    if (region_is_invalid(&new_region)) {
        return NULL;
    }

    last -> next = new_region.addr;
    if (try_merge_with_next(last)) {
        return last;
    }
    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query_capacity, struct block_header* heap_start) {
    if (query_capacity == 0) {
        return NULL;
    }
    struct block_search_result result = try_memalloc_existing(query_capacity, heap_start);
    struct block_header* new_block;
    switch (result.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return result.block;
            break;
        case BSR_REACHED_END_NOT_FOUND:
            new_block = grow_heap(result.block, query_capacity);
            result = try_memalloc_existing(query_capacity, new_block);
            return result.block;
            break;
        case BSR_CORRUPTED:
            return NULL;
    }
    return NULL;
}

void* _malloc( size_t query_capacity ) {
  struct block_header* const addr = memalloc( query_capacity, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
    while (try_merge_with_next(header));
}
